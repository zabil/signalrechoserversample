﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace EchoServerSample.Hubs
{
    public class EchoServerHub : Hub
    {
        private string RoomId => Context.QueryString["roomId"] ?? "default";

        public override async Task OnConnected() {
            await Groups.Add(Context.ConnectionId, RoomId);

            await base.OnConnected();
        }

        public void Send(string message) {
            Clients.Group(RoomId).echo(Context.ConnectionId, message);
        }
    }
}