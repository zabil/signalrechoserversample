﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(EchoServerSample.Startup))]
namespace EchoServerSample
{
    public class Startup
    {
        public void Configuration(IAppBuilder app) {
            app.MapSignalR();            
        }
    }
}
